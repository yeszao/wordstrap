��    B      ,  Y   <      �  
   �  	   �     �     �  8   �          #  $   3     X     d     l     q     y          �     �     �     �     �     �     �               $     B     O     U  |   i     �     �  
                  '     0     6     C     W     _     h     u     �     �     �     �     �     �     �     �  @   �     ;	     O	     X	     d	     z	     �	     �	     �	  	   �	     �	     �	  T   �	     1
     >
     P
  k  g
     �     �     �     �  *   �          2     ?  	   ^     h     u     |     �     �  	   �     �     �     �     �  	   �     �     �     
          3     :     A  �   Q     �     �     �     �          	       	        !     (     /     6  	   E     O     V     ]     j     w     ~     �     �  H   �     �                    *     1     8     E     X     h     o     �     �     �     �                 B   )   =           #   :                               	   
            $   9          '       6         .   1   -   &   @          8             ;             *   ?      (              7       <           /              5       "   4       ,          A   !   +   0                    >       3   %      2       % Comments 1 Comment Apply Coupon Checkout Choose between Bootstrap's container and container-fluid Click here to enter your code Container Width Container width and sidebar defaults Coupon code Coupon: Edit Edit %s Email Fixed width container Footer Full Full width container Have a coupon? Hero Slider Hero Static Leave a comment Left & Right sidebars Left Sidebar Left Sidebar Layout Left and Right Sidebar Layout Left sidebar Login Lost your password? Lost your password? Please enter your username or email address. You will receive a link to create a new password via email. Name No products in the cart. No sidebar Password Posted in %1$s Posts by Price Primary Menu Proceed to Checkout Product Quantity Read More... Remember me Remove this item Reset Password Return To Shop Right Sidebar Right sidebar Search Search &hellip; Search Results for: %s Set sidebar's position. Can either be: right, left, both or none Sidebar Positioning Subtotal Tagged %1$s Theme Layout Settings Total Update Cart Update totals Username or email View Cart Website Your cart is currently empty. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; nounComment post authorby %s post datePosted on %s Project-Id-Version: understrap
Report-Msgid-Bugs-To: http://wordpress.org/support/theme/_s
POT-Creation-Date: 2017-05-21 21:19+0800
POT-Revision-Date: Mon Jul 04 2016 09:13:18 GMT+0200 (CEST)
PO-Revision-Date: 2017-05-22 15:02+0800
Language-Team: 
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.2
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ..
Last-Translator: 
Language: zh_CN
X-Poedit-SearchPath-0: footer.php
X-Poedit-SearchPath-1: header.php
X-Poedit-SearchPath-2: index.php
X-Poedit-SearchPath-3: statichero.php
X-Poedit-SearchPath-4: 404.php
X-Poedit-SearchPath-5: archive.php
X-Poedit-SearchPath-6: search.php
X-Poedit-SearchPath-7: sticky.php
X-Poedit-SearchPath-8: single.php
X-Poedit-SearchPath-9: sidebar.php
X-Poedit-SearchPath-10: comment-form.php
X-Poedit-SearchPath-11: comments.php
X-Poedit-SearchPath-12: content-none.php
X-Poedit-SearchPath-13: content-page.php
X-Poedit-SearchPath-14: content-search.php
X-Poedit-SearchPath-15: content-single.php
X-Poedit-SearchPath-16: content.php
X-Poedit-SearchPath-17: hero.php
X-Poedit-SearchPath-18: page.php
X-Poedit-SearchPath-19: functions.php
X-Poedit-SearchPath-20: page-templates
X-Poedit-SearchPath-21: inc
 %评论 1评论 应用 结算 Bootstrap容器和流体容器之间选择 点击输入优惠码 容器宽度 容器宽度和默认侧边栏 优惠码 优惠券： 编辑 编辑%s 邮箱 固定宽度容器 页脚栏 全宽容器 有优惠码？ 首页幻灯片 首页固定栏 去评论 左右两侧 左侧边栏 左侧边栏布局 左右侧边栏布局 左侧 登录 忘记密码？ 忘记密码？请输入您的用户名或邮箱，确认后我们会给您发一封邮件，点击里面的链接以重置密码。 昵称 购物车暂时没有产品 无 密码 %1$s 作者 价格 主菜单 结算 产品 数量 更多&hellip; 记住我 删除 确认 返回商店 右侧边栏 右侧 搜索 搜索&hellip; “%s”的搜索结果： 侧边栏位置，可选左边、右边、两边、或者无侧边栏。 侧边栏位置 小计 标签：%1$s 主题布局 总额 更新 更新总额 用户名或邮箱 查看购物车 网站 购物车暂时为空。 %2$s 共有 %1$s 条评论 评论 作者：%s 发布于：%s 